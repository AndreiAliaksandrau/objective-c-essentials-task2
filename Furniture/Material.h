//
//  Material.h
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXColor.h"

@interface Material : NSObject
- (id) initWithName:(NSString *)name color:(EXColor) color;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) EXColor color;
@end
