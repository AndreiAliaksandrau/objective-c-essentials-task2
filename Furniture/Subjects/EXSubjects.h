//
//  EXSubjects.h
//  Furniture
//
//  Created by Андрей Александров on 3/20/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#ifndef EXSubjects_h
#define EXSubjects_h

#import "EXBedSubject.h"
#import "EXArmchairSubject.h"
#import "EXCupboardSubject.h"

#endif /* EXSubjects_h */
