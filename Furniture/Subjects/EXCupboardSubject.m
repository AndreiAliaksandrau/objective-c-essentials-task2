//
//  EXCupboardSubject.m
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "EXCupboardSubject.h"

@implementation EXCupboardSubject

- (id)initWithWeight:(float)weight price:(float)price height:(float)height
{
    self = [super initWithWeight:weight price:price];
    if (!self) return nil;
    
    _height = height;
    
    return self;
}

@end
