//
//  EXArmchairSubject.m
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "EXArmchairSubject.h"

@implementation EXArmchairSubject

- (id)initWithWeight:(float)weight price:(float)price numberOfLenght:(int)numberOfLenght
{
    self = [super initWithWeight:weight price:price];
    if (!self) return nil;
    
    _numberOfLenght = numberOfLenght;
    
    return self;
}

@end
