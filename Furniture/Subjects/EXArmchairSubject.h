//
//  EXArmchairSubject.h
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXAbstractSubject.h"

@interface EXArmchairSubject : EXAbstractSubject

- (id)initWithWeight:(float)weight price:(float)price numberOfLenght:(int)numberOfLenght;

@property (readonly, nonatomic) int numberOfLenght;

@end
