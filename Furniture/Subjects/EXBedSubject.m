//
//  EXBedSubject.m
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "EXBedSubject.h"

@implementation EXBedSubject

- (id)initWithWeight:(float)weight price:(float)price heightOfBouncing:(int)heightOfBouncing
{
    self = [super initWithWeight:weight price:price];
    if (!self) return nil;
    
    _heightOfBouncing = heightOfBouncing;
    
    return self;
}

@end
