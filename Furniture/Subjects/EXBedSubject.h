//
//  EXBedSubject.h
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXAbstractSubject.h"

@interface EXBedSubject : EXAbstractSubject

- (id)initWithWeight:(float)weight price:(float)price heightOfBouncing:(int)heightOfBouncing;

@property (readonly, nonatomic) int heightOfBouncing;

@end
