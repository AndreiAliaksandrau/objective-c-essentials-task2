//
//  EXCupboardSubject.h
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXAbstractSubject.h"

@interface EXCupboardSubject : EXAbstractSubject

- (id)initWithWeight:(float)weight price:(float)price height:(float)height;

@property (readonly, nonatomic) float height;

@end
