//
//  EXFurnitureSet.m
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "EXFurnitureSet.h"

@interface EXFurnitureSet () {
}

@property (readonly, strong, nonatomic) NSMutableArray <ISubject> *items;
@end

@implementation EXFurnitureSet

@synthesize items;

- (instancetype) init {
    self = [super init];
    
    items = [[NSMutableArray <ISubject> alloc] init];
    
    return self;
}

- (NSMutableArray*) getItems {
    return items;
}

- (NSMutableArray *) getItemsWithMaterial:(Material *) material {
    NSMutableArray <EXAbstractSubject *> *results = [[NSMutableArray <EXAbstractSubject *> alloc] init];
    
    for(EXAbstractSubject *sub in items) {
        for (Material *mat in sub.materials) {
            if (mat == material) {
                [results addObject:sub];
            }
        }
    }
    
    return results;
}

- (void) addItem:(EXAbstractSubject *)subject {
    [items addObject: subject];
}

- (NSMutableArray *) getMaterials {
    NSMutableArray <Material *> *results = [[NSMutableArray alloc] init];
    
    for(EXAbstractSubject *sub in items) {
        for (Material *mat in sub.materials) {
            [results addObject:mat];
        }
    }
    
    return results;
}

@end
