//
//  EXColor.m
//  Furniture
//
//  Created by Андрей Александров on 3/20/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "EXColor.h"

@implementation EXColorConverter
-(NSString*) toString:(EXColor) color {
    NSString *result = nil;
    
    switch(color) {
        case 0:
            result = @"Black";
            break;
        case 1:
            result = @"Blue";
            break;
        case 2:
            result = @"Green";
            break;
        case 3:
            result = @"Brown";
            break;
        case 4:
            result = @"Red";
            break;
        case 5:
            result = @"White";
            break;
        default:
            result = @"Black";
    }
    
    return result;
}
@end
