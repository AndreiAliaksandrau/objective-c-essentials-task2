//
//  EXColor.h
//  Furniture
//
//  Created by Андрей Александров on 3/20/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    Black = 0,
    Blue = 1,
    Green = 2,
    Brown = 3,
    Red = 4,
    White = 5
} EXColor;

@interface EXColorConverter : NSObject
-(NSString*) toString:(EXColor) color;
@end
