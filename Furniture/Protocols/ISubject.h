//
//  ISubject.h
//  Furniture
//
//  Created by Андрей Александров on 3/22/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#ifndef ISubject_h
#define ISubject_h

#import "Material.h"

@protocol ISubject

@required

@property (readwrite, strong, nonatomic) NSMutableArray <Material *> *materials;
@property (readonly, nonatomic) float weight;
@property (readonly, nonatomic) float price;

@end

#endif /* ISubject_h */
