//
//  EXAbstractSubject.h
//  Furniture
//
//  Created by Андрей Александров on 3/20/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISubject.h"

@interface EXAbstractSubject : NSObject <ISubject>
- (id)initWithWeight:(float)weight;
- (id)initWithWeight:(float)weight price:(float)price;

@property (readwrite, strong, nonatomic) NSMutableArray <Material *> *materials;
@property (readonly, nonatomic) float weight;
@property (readonly, nonatomic) float price;
@end
