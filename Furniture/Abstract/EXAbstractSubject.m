//
//  EXAbstractSubject.m
//  Furniture
//
//  Created by Андрей Александров on 3/20/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "EXAbstractSubject.h"

@implementation EXAbstractSubject

- (id)initWithWeight:(float)weight
{
    self = [super init];
    if (!self) return nil;
    
    _materials = [[NSMutableArray alloc] init];
    _weight =  weight;
    
    return self;
}

- (id)initWithWeight:(float)weight price:(float)price
{
    self = [super init];
    if (!self) return nil;
    
    _materials = [[NSMutableArray alloc] init];
    _weight = weight;
    _price = price;
    
    return self;
}
@end
