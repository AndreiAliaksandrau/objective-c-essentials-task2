//
//  EXFurnitureSet.h
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXAbstractSubject.h"

@interface EXFurnitureSet : NSObject
//<ISubject>

- (NSMutableArray *) getItems;
- (NSMutableArray *) getItemsWithMaterial:(Material *) material;
- (void) addItem:(EXAbstractSubject *) subject;
- (NSMutableArray *) getMaterials;

//@property (readwrite, strong, nonatomic) NSMutableArray <Material *> *materials;
//@property (readonly, strong, nonatomic) NSNumber *weight;
//@property (readonly, strong, nonatomic) NSDecimalNumber *price;

@end
