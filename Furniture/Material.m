//
//  Material.m
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import "Material.h"

@implementation Material
- (id) initWithName:(NSString *)name color:(EXColor) color {
    self = [super init];
    if (!self) return nil;

    _name = name;
    _color = color;
    
    return self;
}
@end
