//
//  main.m
//  Furniture
//
//  Created by Андрей Александров on 3/19/18.
//  Copyright © 2018 AndreiAliaksandrau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXFurnitureSet.h"
#import "Material.h"
#import "EXSubjects.h"
#import "ISubject.h"
#import "EXColor.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        EXFurnitureSet *furniture = [[EXFurnitureSet alloc] init];

        EXAbstractSubject *absSub = [[EXAbstractSubject alloc] initWithWeight:100 price:5.5];
        EXBedSubject *bedSub = [[EXBedSubject alloc] initWithWeight:90 price:50.3 heightOfBouncing:2];
        EXArmchairSubject *armchairSub = [[EXArmchairSubject alloc] initWithWeight:170 price:23.5 numberOfLenght:4];
        
        EXCupboardSubject *cupboardSub = [[EXCupboardSubject alloc] initWithWeight:132 price:105.7 height:3];
        
        Material *wood = [[Material alloc] initWithName:@"Wood" color:Red];
        Material *iron = [[Material alloc] initWithName:@"Iron" color:Black];
        Material *plastic = [[Material alloc] initWithName:@"Plastic" color:White];

        [absSub.materials addObject:wood];
        
        [bedSub.materials addObject:iron];
        [bedSub.materials addObject:wood];
        
        [armchairSub.materials addObject:plastic];
        [cupboardSub.materials addObject:plastic];
        
        [furniture addItem:absSub];
        [furniture addItem:bedSub];
        [furniture addItem:armchairSub];
        
        for (EXAbstractSubject *sub in [furniture getItems]) {
            NSLog(@"Weight: %.2f, Price: %.2f", sub.weight, sub.price);
        }
        
        NSLog(@"Results:");
        
        NSMutableArray *results = [furniture getItemsWithMaterial:wood];
        for (EXAbstractSubject *sub in results) {
            NSLog(@"Weight: %.2f, Price: %.2f", sub.weight, sub.price);
        }
    }
    return 0;
}
